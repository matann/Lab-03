# Lab-03

# Microservices Workshop
Lab 03: Create a CI pipeline with VSTS build

---

## Create a CI pipeline for each service:

- Create a build and select the repository as source:

 ![Image 1](images/lab03-1.png)

- Select "Empty Job" and set the agent pool as "docker-agents":

 ![Image 1](images/lab03-2.png)

- Configure the build process:

 ![Image 1](images/lab03-3.png)

```
Name: npm install
Script: npm install
```
```
Name: npm test
Script: npm run test
```
```
Name: docker build
Script: docker build -t leonjalfon1/division-service:$(Build.BuildId) .
```
```
Name: docker tag
Script: docker tag leonjalfon1/division-service:$(Build.BuildId) leonjalfon1/division-service:latest
```
```
Name: docker login
Script: docker login -u leonjalfon1 -p Ljp9920051d $(password)
```
```
Name: docker push
Script: docker push leonjalfon1/division-service:$(Build.BuildId)
		docker push leonjalfon1/division-service:latest
```

- Add a secure variable for the docker hub password

- Enable continuous integration under "Triggers"

- Queue the build an ensure that was configured correctly
